package me.lpk.backdoor.util;

/**
 * So I totally forgot that most servers won't be running the latest java. So
 * here's a shitty backport for java 8 lambdas.
 */
public class Backport {
	public static abstract class Predicate<V> {
		public abstract boolean test(V arg);
	}

	public static abstract class Consumer<V> {
		public abstract void accept(V arg);
	}
}
