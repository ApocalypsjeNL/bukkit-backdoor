package me.lpk.backdoor.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Array allowing a basic get or default operation.
 */
public class StringArray extends ArrayList<String> {
	private static final long serialVersionUID = 666L;

	/**
	 * Creates a StringArray from the given list<String>.
	 *
	 * @param list
	 * @return
	 */
	public static StringArray from(List<String> list) {
		StringArray s = new StringArray();
		if (list != null) {
			s.addAll(list);
		}
		return s;
	}

	/**
	 * Return the value at the given index or return a default value by the
	 * given parameter.
	 *
	 * @param index
	 * @param def
	 * @return
	 */
	public String getOrDefault(int index, String def) {
		if (index >= size()) {
			return def;
		}
		return get(index);
	}

	/**
	 * Return the string array as a single string.
	 *
	 * @param split
	 *            String that replaces spaces between values.
	 * @return
	 */
	public String toSingle(String split) {
		return toSingle(0, split);
	}

	/**
	 * Return the string array as a single string. Skips the values before the
	 * given starting index.
	 *
	 * @param startIndex
	 * @param split
	 *            String that replaces spaces between values.
	 * @return
	 */
	public String toSingle(int startIndex, String split) {
		StringBuilder sb = new StringBuilder();
		for (int i = startIndex; i < size(); i++) {
			if (i >= size() - 1) {
				sb.append(get(i));
			} else {
				sb.append(get(i) + split);
			}
		}
		return sb.toString();
	}
}
