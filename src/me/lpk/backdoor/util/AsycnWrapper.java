package me.lpk.backdoor.util;

import java.lang.reflect.Field;

/**
 * Wrapper for org.spigotmc.AsyncCatcher. Class does not exist in normal bukkit
 * servers, so reflection is required to build.
 */
public class AsycnWrapper {
	private static Field enabled;

	public static void setAsync(boolean value) {
		if (enabled != null) {
			try {
				enabled.set(null, value);
			} catch (Exception e) {}
		}
	}

	static {
		try {
			Class<?> asyncClass = Class.forName("org.spigotmc.AsyncCatcher");
			enabled = asyncClass.getDeclaredField("enabled");
			enabled.setAccessible(true);
		} catch (Exception e) {}
	}
}
