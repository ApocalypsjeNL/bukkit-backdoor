package me.lpk.backdoor.cheats;

import me.lpk.backdoor.util.StringArray;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.UUID;

public class Cheat {
	public boolean status;
	public final String name;
	protected final String id;

	public Cheat(String name, UUID id) {
		this.name = name;
		this.id = id.toString();
	}

	public Player getPlayer() {
		return Bukkit.getPlayer(UUID.fromString(id));
	}

	/**
	 * Toggles the cheat.
	 */
	public void toggle(StringArray args) {
		status = !status;
		getPlayer().sendMessage(ChatColor.BLUE + "[" + name + "]: " + ChatColor.GRAY + (status ? "enabled" : "disabled"));
		if (status) {
			onEnable(args);
		} else {
			onDisable(args);
		}
	}

	/**
	 * Called when the cheat is enabled.
	 *
	 * @param args
	 */
	public void onEnable(StringArray args) {}

	/**
	 * Called when the cheat is disabled.
	 *
	 * @param args
	 */
	public void onDisable(StringArray args) {}
}
